import io.scenarium.matlab.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

open module io.scenarium.simulink {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.flow;
	requires com.sun.jna;

	exports io.scenarium.matlab;
	exports io.scenarium.matlab.model;
	exports io.scenarium.matlab.operator.wrapper;

}
