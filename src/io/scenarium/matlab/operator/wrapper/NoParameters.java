package io.scenarium.matlab.operator.wrapper;

import java.util.List;

import io.beanmanager.editors.TransientProperty;
import io.scenarium.matlab.model.JNAStructure;

public class NoParameters extends JNAStructure {
	public NoParameters() {}

	@TransientProperty
	public int notAParam;

	public int getNotAParam() {
		return -1;
	}

	public void setNotAParam(int notAParam) {}

	@Override
	protected List<String> getFieldOrder() {
		return List.of("notAParam");
	}

	@Override
	public String toString() {
		return "";
	}
}
