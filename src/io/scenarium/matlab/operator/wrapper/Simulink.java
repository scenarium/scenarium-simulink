/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.matlab.operator.wrapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import io.beanmanager.BeanManager;
import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.UpdatableViewBean;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.editors.container.BeanInfo;
import io.beanmanager.editors.container.DynamicBeanInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.core.tools.SchedulerUtils;
import io.scenarium.core.tools.dynamiccompilation.CompilationException;
import io.scenarium.core.tools.dynamiccompilation.InlineCompiler;
import io.scenarium.flow.BlockInfo;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.matlab.internal.Log;
import io.scenarium.matlab.model.JNAStructure;
import io.scenarium.matlab.model.StepMode;

import com.sun.jna.Function;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

@BlockInfo(info = "This block can execute simulink diagram compiled with Matlab Embedded coder")
public class Simulink extends EvolvedOperator implements DynamicEnableBean, UpdatableViewBean {
	private static final String GROUPDELIMITER = "_";
	@PropertyInfo(index = 0, info = "header (*.h) of the generated diaram")
	private File header;
	@PropertyInfo(index = 1, info = "library (*.so, *.dll) of the generated diaram")
	private File library;
	@PropertyInfo(index = 2, info = "FIXED: The block is schedule at fixed time rate defined by the parameter fixedStepSize.\nVARIABLEWITHALLENTRIES: The block wait all inputs.\nVARIABLEWITHONEENTRIES: The block wait for at least one input.\n")
	private StepMode stepMode = StepMode.VARIABLEWITHONEENTRIES;
	@PropertyInfo(index = 3, unit = "ms")
	@NumberInfo(min = 0)
	private int fixedStepSize = 50;
	@PropertyInfo(index = 4, info = "Prefix for IHM parameters")
	private String parameterPrefix = "";
	@PropertyInfo(index = 5, nullable = false, info = "Parameters of the diaram")
	@BeanInfo(alwaysExtend = true, inline = true)
	@DynamicBeanInfo(possibleSubclassesMethodName = "getStructType")
	private JNAStructure parameters;
	private InlineCompiler ic;
	private IOPDescriptor iop;
	private Structure in;
	private Structure out;
	private NativeLibrary lib;

	private Function testStep;

	private Object[] oldInputs;

	private long maxTs = -1;

	private ScheduledExecutorService fixedModeTimer;

	private static Class<?> getType(String matlabType) {
		switch (matlabType) {
		case "int8_T":
			return byte.class;
		case "uint8_T":
			return byte.class;
		case "int16_T":
			return short.class;
		case "uint16_T":
			return short.class;
		case "int32_T":
			return int.class;
		case "uint32_T":
			return int.class;
		case "int64_T":
			return long.class;
		case "uint64_T":
			return long.class;
		case "real32_T":
			return float.class;
		case "real64_T":
			return double.class;
		case "real_T":
			return double.class;// isArray ? double[].class : double.class;
		case "time_T":
			return double.class;
		case "boolean_T":
			return byte.class;
		case "int_T":
			return int.class;
		case "uint_T":
			return int.class;
		case "ulong_T":
			return long.class;
		case "char_T":
			return byte.class;
		case "uchar_T":
			return byte.class;
		case "byte_T":
			return byte.class;
		default:
			return null;
		}
	}

	public static void main(String[] args) {
		Simulink simulink = new Simulink();
		simulink.header = new File("/home/revilloud/Dropbox/TestSimulink/test_ert_shrlib_rtw/test.h");
		simulink.library = new File("/home/revilloud/Dropbox/TestSimulink/test.so");
		try {
			simulink.updateIOStructure();
			simulink.birth();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Variable processExtern(String extern) {
		int indexOfEquals = extern.indexOf("=");
		if (indexOfEquals != -1)
			extern = extern.substring(0, indexOfEquals - 1).trim();
		int indexOfParamName = extern.lastIndexOf(" ");
		String name = extern.substring(indexOfParamName + 1);
		String type = extern.substring(0, indexOfParamName);
		int indexOfParamType = type.lastIndexOf(" ");
		type = type.substring(indexOfParamType + 1);
		int indexOfArray = name.indexOf("[");
		int size;
		if (indexOfArray != -1) {
			String sSize = name.substring(indexOfArray + 1);
			size = Integer.parseInt(sSize.substring(0, sSize.indexOf("]")));
			name = name.substring(0, indexOfArray);
		} else
			size = -1;
		return new Variable(name, type, size);
	}

	private static CStructDescriptor processTypedefStruct(StringBuilder struct) {
		int indexOfBeginHeader = struct.indexOf("{");
		int indexOfEndHeader = struct.indexOf("}");
		String header = struct.substring(0, indexOfBeginHeader).trim();
		String parameters = struct.substring(indexOfBeginHeader + 1, indexOfEndHeader).trim();
		String foot = struct.substring(indexOfEndHeader + 1).trim();
		String structName;
		if (header.startsWith("typedef"))
			structName = foot;
		else {
			int tagNameIndex = header.lastIndexOf(" ");
			structName = tagNameIndex == -1 ? "" : header.substring(tagNameIndex).trim();
		}
		StringTokenizer st = new StringTokenizer(parameters, ";");
		ArrayList<PrimitiveVariable> params = new ArrayList<>();
		while (st.hasMoreTokens()) {
			String param = st.nextToken().trim();
			int indexOfEquals = param.indexOf("=");
			if (indexOfEquals != -1)
				param = param.substring(0, indexOfEquals - 1).trim();
			int indexOfParamName = param.lastIndexOf(" ");
			String name = param.substring(indexOfParamName + 1);
			param = param.substring(0, indexOfParamName);
			int indexOfParamType = param.lastIndexOf(" ");
			int indexOfArray = name.indexOf("[");
			int size;
			if (indexOfArray != -1) {
				String sSize = name.substring(indexOfArray + 1);
				size = Integer.parseInt(sSize.substring(0, sSize.indexOf("]")));
				name = name.substring(0, indexOfArray);
			} else
				size = PrimitiveVariable.NOTANARRAY;
			Class<?> type = getType(indexOfParamType == -1 ? param : param.substring(0, indexOfParamType + 1));
			if (type != null)
				params.add(new PrimitiveVariable(name, type, size));
		}
		return new CStructDescriptor(structName, params);
	}

	@Override
	public void birth() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		load();
		this.lib.getFunction(this.iop.initializeMethodName).invokeVoid(null);
		this.testStep = this.lib.getFunction(this.iop.stepMethodName);
		this.oldInputs = new Object[this.iop.inputs.params.size()];
		if (this.stepMode == StepMode.FIXED)
			onStart(() -> {
				this.fixedModeTimer = SchedulerUtils.getTimer(getBlockName() + "-Timer");
				this.fixedModeTimer.scheduleAtFixedRate(() -> {
					if (Simulink.this.stepMode != StepMode.FIXED) {
						this.fixedModeTimer.shutdownNow();
						return;
					}
					nextStep();
				}, 0, this.fixedStepSize, TimeUnit.MILLISECONDS);
			});
	}

	@SuppressWarnings("unchecked")
	private ArrayList<Class<? extends Structure>> createAndLoadJNAJavaClass(IOPDescriptor iOPDescriptor) {
		String[] inCode = generateIOCode(iOPDescriptor.inputs);
		String[] outCode = generateIOCode(iOPDescriptor.outputs);
		String[] paramCode = iOPDescriptor.parameters != null ? generateParameterCode(iOPDescriptor.parameters) : null;
		try {
			this.ic = paramCode == null ? new InlineCompiler(new String[] { inCode[0], outCode[0] }, new String[] { inCode[1], outCode[1] })
					: new InlineCompiler(new String[] { inCode[0], outCode[0], paramCode[0] }, new String[] { inCode[1], outCode[1], paramCode[1] });
			ArrayList<Class<? extends Structure>> c = new ArrayList<>();
			for (Class<?> class1 : this.ic.getCompileClass())
				c.add((Class<? extends Structure>) class1);
			return c;
		} catch (CompilationException e) {
			for (Diagnostic<? extends JavaFileObject> error : e.getErrors())
				Log.error("Line: " + error.getLineNumber() + ": " + error.getMessage(Locale.getDefault()));
			return null;
		}
	}

	@Override
	public void death() throws Exception {
		this.lib.getFunction(this.iop.terminateMethodName).invokeVoid(null);
		this.oldInputs = null;
		this.maxTs = -1;
		stopTimer();
	}

	private void dispose() {
		if (this.lib != null) {
			this.lib.dispose();
			this.lib = null;
		}
		if (this.ic != null) {
			this.ic.close();
			this.ic = null;
		}
		this.iop = null;
		this.parameters = null;
		this.in = null;
		this.out = null;
	}

	private static String[] generateIOCode(CStructDescriptor struct) {
		ArrayList<PrimitiveVariable> params = struct.params;
		StringBuilder sb = new StringBuilder();
		String pName = "simulink";
		String className = pName + "." + struct.structName;
		sb.append("package " + pName + ";\n");
		sb.append("import " + Arrays.class.getName() + ";\n");
		sb.append("import " + List.class.getName() + ";\n");
		sb.append("import " + Pointer.class.getName() + ";\n");
		sb.append("import " + Structure.class.getName() + ";\n");
		sb.append("import " + PropertyInfo.class.getName() + ";\n");
		sb.append("import " + BeanInfo.class.getName() + ";\n");

		sb.append("public class " + struct.structName + " extends Structure{\n");
		for (PrimitiveVariable var : params) {
			sb.append("\tpublic " + var.type);
			if (var.size == -1)
				sb.append(" " + var.name + ";\n");
			else
				sb.append("[] " + var.name + " = new " + var.type + "[" + var.size + "];\n");
		}
		sb.append("\tpublic " + struct.structName + "() {\n");
		sb.append("\t\tsuper();\n");
		sb.append("\t}\n");

		sb.append("\tpublic void setPointer(Pointer p) {\n");
		sb.append("\t\tuseMemory(p);\n");
		sb.append("\t}\n");

		HashSet<String> visited = new HashSet<>();
		for (PrimitiveVariable var : params) {
			String upName = var.name;// .name.substring(0, 1).toUpperCase() + var.name.substring(1);
			if (!visited.add(upName))
				continue;
			String type = var.type + (var.size == -1 ? "" : "[]");
			sb.append("\tpublic " + type + " get" + upName + "() {\n");
			sb.append("\t\treturn " + var.name + ";\n");
			sb.append("\t}\n");

			sb.append("\tpublic void set" + upName + "(" + type + " " + var.name + ") {\n");
			sb.append("\t\tthis." + var.name + " = " + var.name + ";\n");
			sb.append("\t\twriteField(\"" + var.name + "\");\n");
			sb.append("\t}\n");
		}

		sb.append("\tprotected List<String> getFieldOrder() {\n");
		sb.append("\t\treturn Arrays.asList(");
		if (!params.isEmpty()) {
			for (int i = 0; i < params.size() - 1; i++)
				sb.append("\"" + params.get(i).name + "\", ");
			sb.append("\"" + params.get(params.size() - 1).name + "\"");
		}
		sb.append(");\n");
		sb.append("\t}\n");

		sb.append("}\n");
		return new String[] { className, sb.toString() };
	}

	@SuppressWarnings("unchecked")
	private String[] generateParameterCode(CStructDescriptor struct) {
		ArrayList<PrimitiveVariable> params = struct.params;
		HashMap<String, Object> paramMap = new HashMap<>();
		for (PrimitiveVariable param : params) {
			String name = param.name;
			String[] arbo = name.split(GROUPDELIMITER);
			// Check that all elements are valid java name
			int nbElement = arbo.length;
			for (int i = 1; i < arbo.length; i++)
				if (!Character.isJavaIdentifierStart(arbo[i].charAt(0))) {
					int id = i - 1;
					while (arbo[id] == null)
						id--;
					arbo[id] += "_" + arbo[i];
					arbo[i] = null;
					nbElement--;
				}
			// Remove null elements in array
			if (arbo.length != nbElement) {
				String[] newArbo = new String[nbElement];
				int newArboIndex = 0;
				for (int i = 0; i < arbo.length; i++)
					if (arbo[i] != null)
						newArbo[newArboIndex++] = arbo[i];
				arbo = newArbo;
			}
			for (int i = 0; i < arbo.length - 1; i++)
				arbo[i] += GROUPDELIMITER;
			if (name.endsWith(GROUPDELIMITER))
				arbo[arbo.length - 1] += GROUPDELIMITER;
			HashMap<String, Object> node = paramMap;
			Object nodeLvl = null;
			for (int i = 0; i < arbo.length - 1; i++) {
				String lvl = arbo[i];
				nodeLvl = node.get(lvl);
				if (nodeLvl == null) {
					nodeLvl = new HashMap<>();
					node.put(lvl, nodeLvl);
				} else if (nodeLvl instanceof String) {
					Log.error("conflit: " + nodeLvl + " is a group and a value...");
					nodeLvl = new HashMap<>();
					node.put(lvl, nodeLvl);
				}
				node = (HashMap<String, Object>) nodeLvl;
			}
			node.put(arbo[arbo.length - 1], param);
		}
		LinkedList<HashMap<String, Object>> todoNodes = new LinkedList<>();
		LinkedList<String> todoNames = new LinkedList<>();
		LinkedList<HashMap<String, Object>> todoFatherNode = new LinkedList<>();
		LinkedList<String> todoFatherName = new LinkedList<>();
		for (String objName : paramMap.keySet()) {
			Object obj = paramMap.get(objName);
			if (obj instanceof HashMap) {
				todoFatherNode.add(paramMap);
				todoFatherName.add("");
				todoNodes.add((HashMap<String, Object>) obj);
				todoNames.add(objName);
			}
		}
		while (!todoNodes.isEmpty()) {
			HashMap<String, Object> grandFatherNode = todoFatherNode.pop();
			String grandFatherName = todoFatherName.pop();
			HashMap<String, Object> fatherNode = todoNodes.pop();
			String fatherName = todoNames.pop();
			Set<String> sonNodesName = fatherNode.keySet();
			if (sonNodesName.size() == 1 || grandFatherName.equals(fatherName)) {
				String sonName = fatherNode.keySet().iterator().next();
				Object son = fatherNode.get(sonName);
				String newNodeName = fatherName + sonName;
				grandFatherNode.remove(fatherName);
				grandFatherNode.put(newNodeName, son);
				if (son instanceof HashMap) {
					todoFatherNode.push(grandFatherNode);
					todoFatherName.push(fatherName);
					todoNodes.push((HashMap<String, Object>) son);
					todoNames.push(newNodeName);
				}
			} else
				for (String sonName : sonNodesName) {
					Object sonNode = fatherNode.get(sonName);
					if (sonNode instanceof HashMap) {
						todoFatherNode.push(fatherNode);
						todoFatherName.push(fatherName);
						todoNodes.push((HashMap<String, Object>) sonNode);
						todoNames.push(sonName);
					}
				}
		}
		// displayMap(paramMap, 0);
		StringBuilder sb = new StringBuilder();
		String pName = "simulink";
		String className = pName + "." + struct.structName;
		sb.append("package " + pName + ";\n");
		sb.append("import " + Arrays.class.getName() + ";\n");
		sb.append("import " + List.class.getName() + ";\n");
		sb.append("import " + Pointer.class.getName() + ";\n");
		sb.append("import " + JNAStructure.class.getName() + ";\n");
		sb.append("import " + PropertyInfo.class.getName() + ";\n");
		sb.append("import " + BeanInfo.class.getName() + ";\n");
		sb.append("public class " + struct.structName + " extends JNAStructure {\n");
		for (PrimitiveVariable var : params) { // déclaration des variable, on change rien
			sb.append("\tpublic " + var.type);
			if (var.size == -1)
				sb.append(" " + var.name + ";\n");
			else
				sb.append("[] " + var.name + " = new " + var.type + "[" + var.size + "];\n");
		}
		sb.append("\tpublic " + struct.structName + "() {\n");
		sb.append("\t\tsuper();\n");
		sb.append("\t}\n");
		sb.append("\tpublic void setPointer(Pointer p) {\n");
		sb.append("\t\tuseMemory(p);\n");
		sb.append("\t}\n");
		HashSet<String> visited = new HashSet<>();
		paramMap.forEach((name, element) -> {
			if (element instanceof HashMap) {
				if (this.parameterPrefix == null || name.startsWith(this.parameterPrefix))
					writeClass(sb, struct.structName, (HashMap<String, Object>) element, name, 1);
			} else {
				PrimitiveVariable var = (PrimitiveVariable) element;
				String upName = getVarName(var.name);// .substring(0, 1).toUpperCase() + var.name.substring(1);
				if (!visited.contains(upName) && (this.parameterPrefix == null || var.name.startsWith(this.parameterPrefix))) {
					visited.add(upName);
					String type = var.type + (var.size == -1 ? "" : "[]");
					sb.append("\tpublic " + type + " get" + upName + "() {\n");
					sb.append("\t\treturn " + var.name + ";\n");
					sb.append("\t}\n");
					sb.append("\tpublic void set" + upName + "(" + type + " " + var.name + ") {\n");
					sb.append("\t\tthis." + var.name + " = " + var.name + ";\n");
					sb.append("\t\twriteField(\"" + var.name + "\");\n");
					sb.append("\t}\n");
				}
			}
		});
		visited.clear();
		sb.append("\tprotected List<String> getFieldOrder() {\n");
		sb.append("\t\treturn Arrays.asList(");
		for (int i = 0; i < params.size() - 1; i++)
			sb.append("\"" + params.get(i).name + "\", ");
		sb.append("\"" + params.get(params.size() - 1).name + "\"");
		sb.append(");\n");
		sb.append("\t}\n");

		sb.append("\tpublic String toString() {\n");
		sb.append("\t\treturn \"Simulink parameters\";\n");
		sb.append("\t}\n");

		sb.append("}\n");
		return new String[] { className, sb.toString() };
	}

	private String getVarName(String name) {
		return "P" + name;
	}

	public int getFixedStepSize() {
		return this.fixedStepSize;
	}

	public File getHeader() {
		return this.header;
	}

	public File getLibrary() {
		return this.library;
	}

	// @SuppressWarnings("unchecked")
	// private void displayMap(HashMap<String, Object> paramMap, int i) {
	// String indent = "";
	// for (int j = 0; j < i; j++) {
	// indent += "\t";
	// }
	// for (String nodeName : paramMap.keySet()) {
	// Object node = paramMap.get(nodeName);
	// if (node instanceof HashMap) {
	// Log.info(indent + nodeName + ": ");
	// displayMap((HashMap<String, Object>) node, i + 1);
	// } else
	// Log.info(indent + nodeName);
	// }
	// }

	public String getParameterPrefix() {
		return this.parameterPrefix;
	}

	public JNAStructure getParameters() {
		try {
			load();
		} catch (InstantiationException | UnsatisfiedLinkError | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
				| NullPointerException | IOException e) {
			if (!(e instanceof UnsatisfiedLinkError) && this.library != null && this.library.exists())
				Log.error("Error while loading the library: " + e.getMessage());
		}
		if (this.parameters != null) {
			if (BeanEditor.getBeanDesc(this.parameters) == null)
				BeanEditor.registerBean(this.parameters, BeanManager.defaultDir);
			this.parameters.read();
		}
		return this.parameters;
	}

	public StepMode getStepMode() {
		return this.stepMode;
	}

	public Class<?>[] getStructType() {
		try {
			if (this.iop == null)
				this.iop = parseHeader(this.header);
			load();
			if (this.parameters != null)
				return new Class<?>[] { this.parameters.getClass() };
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
			if (!(e instanceof NoSuchFileException))
				e.printStackTrace();
		}
		return new Class<?>[] { NoParameters.class };
	}

	@Override
	public void updateIOStructure() throws IOException {
		if (this.header == null || !this.header.exists() || this.library == null || !this.library.exists()) {
			updateInputs(new String[0], new Class<?>[0]);
			updateOutputs(new String[0], new Class<?>[0]);
			return;
		}
		if (this.iop == null)
			this.iop = parseHeader(this.header);
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		for (PrimitiveVariable var : this.iop.inputs.params) {
			names.add(var.name);
			types.add(var.size == -1 ? var.type : var.type.arrayType());
		}
		updateInputs(names.toArray(String[]::new), types.toArray(Class[]::new));

		names.clear();
		types.clear();
		for (PrimitiveVariable var : this.iop.outputs.params) {
			names.add(var.name);
			types.add(var.size == -1 ? var.type : var.type.arrayType());
		}
		updateOutputs(names.toArray(String[]::new), types.toArray(Class[]::new));
	}

	private void load() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		if (this.lib == null && this.library != null && !this.library.getPath().isBlank()) {
			try {
				this.lib = NativeLibrary.getInstance(this.library.getAbsolutePath()); // Ajouter class Loader pour décharger?
			} catch (Exception | UnsatisfiedLinkError e) {
				Log.error("Not able to load library: " + this.library.getAbsolutePath() + e.getMessage());
				return;
			}
			if (this.iop == null)
				this.iop = parseHeader(this.header);
			if (this.iop == null)
				return;
			ArrayList<Class<? extends Structure>> c = createAndLoadJNAJavaClass(this.iop);
			if (c != null) {
				this.in = c.get(0).getConstructor().newInstance();
				this.out = c.get(1).getConstructor().newInstance();
				this.in.getClass().getMethod("setPointer", Pointer.class).invoke(this.in, this.lib.getGlobalVariableAddress(this.iop.inputs.variablename));
				this.out.getClass().getMethod("setPointer", Pointer.class).invoke(this.out, this.lib.getGlobalVariableAddress(this.iop.outputs.variablename));
				// if (this.parameters != null) {
				if (c.size() == 3) {
					this.parameters = (JNAStructure) c.get(2).getConstructor().newInstance();
					this.parameters.getClass().getMethod("setPointer", Pointer.class).invoke(this.parameters, this.lib.getGlobalVariableAddress(this.iop.parameters.variablename));
					this.parameters.read();
				}
			}
		}
	}

	private void nextStep() {
		// Log.info("invoke");
		// long time = System.currentTimeMillis();
		this.testStep.invokeVoid(null);
		// Log.info("invoke done, execution time: " + (System.currentTimeMillis() - time));
		ArrayList<PrimitiveVariable> outParams = this.iop.outputs.params;
		Object[] outputs = new Object[outParams.size()];
		for (int i = 0; i < outputs.length; i++)
			outputs[i] = this.out.readField(outParams.get(i).name);
		triggerOutput(outputs, this.maxTs);
	}

	private static IOPDescriptor parseHeader(File header) throws IOException {
		if (header != null && !header.getPath().isBlank()) {
			if (header.isDirectory() || !header.exists()) {
				Log.error("Simulink header: " + header + " is not a valid file");
				return null;
			}

			ArrayList<CStructDescriptor> structs = new ArrayList<>();
			ArrayList<Variable> externVariable = new ArrayList<>();

			String initializeMethodName = null;
			String stepMethodName = null;
			String terminateMethodName = null;

			List<String> lines = Files.readAllLines(header.toPath());
			StringBuilder typedefStructLines = null;
			StringBuilder extern = null;
			boolean isEndOfStructReach = false;
			boolean isInCommentMode = false;
			nextLine: for (String line : lines) {
				line = line.trim();
				int beginCommentInLine = 0;
				nextVerif: while (true) {
					if (isInCommentMode) {
						int indexOfCommentEnd = line.indexOf("*/");
						if (indexOfCommentEnd == -1) {
							if (beginCommentInLine == 0)
								continue nextLine;
							line = line.substring(0, beginCommentInLine).trim();
							break nextVerif;
						}
						String part1 = line.substring(0, beginCommentInLine);
						line = (part1 + line.substring(indexOfCommentEnd + 2)).trim();
					}
					int indexOfComment = line.indexOf("/*");
					if (indexOfComment != -1) {
						isInCommentMode = true;
						beginCommentInLine = indexOfComment;
						continue;
					}
					isInCommentMode = false;
					break;
				}
				if (line.isEmpty())
					continue;
				int indexOfComment = line.indexOf("//"); // Remove // comment
				if (indexOfComment != -1)
					line = line.substring(0, indexOfComment);

				if (typedefStructLines != null) {
					if (!isEndOfStructReach && line.contains("}"))
						isEndOfStructReach = true;
					if (isEndOfStructReach && line.contains(";")) {
						line = line.substring(0, line.indexOf(";"));
						typedefStructLines.append(" " + line);
						structs.add(processTypedefStruct(typedefStructLines));
						isEndOfStructReach = false;
						typedefStructLines = null;
					} else
						typedefStructLines.append(" " + line);
				} else if (extern != null) {
					if (line.contains(";")) {
						extern.append(" " + line.substring(0, line.indexOf(";")));
						if (extern.indexOf("(") == -1)
							externVariable.add(processExtern(extern.toString().trim()));
						extern = null;
					} else
						extern.append(" " + line);
				} else if (line.startsWith("typedef struct") || line.startsWith("struct")) {
					typedefStructLines = new StringBuilder();
					typedefStructLines.append(line);
					if (!isEndOfStructReach && line.contains("}"))
						isEndOfStructReach = true;
					if (isEndOfStructReach && line.contains(";")) {
						line = line.substring(0, line.indexOf(";") + 1);
						structs.add(processTypedefStruct(typedefStructLines));
						isEndOfStructReach = false;
						typedefStructLines = null;
					}
				} else if (line.startsWith("extern")) {
					extern = new StringBuilder(line.substring(0, line.indexOf(";")).replaceAll("__declspec\\(dllexport\\)", ""));
					if (line.contains(";")) {
						if (extern.indexOf("(") == -1)
							externVariable.add(processExtern(extern.toString().trim()));
						else {
							StringTokenizer st = new StringTokenizer(extern.toString(), " ");
							if (st.hasMoreTokens() && st.nextToken().equals("extern") && st.hasMoreTokens() && st.nextToken().equals("void") && st.hasMoreTokens()) {
								String funcName = extern.substring(extern.indexOf("void") + 4).trim();
								int indexOfParan = funcName.indexOf("(");
								if (indexOfParan != -1) {
									String parameters = funcName.substring(indexOfParan + 1, funcName.lastIndexOf(")")).trim();
									if (parameters.equals("void") || parameters.isEmpty()) {
										String methodeName = funcName.substring(0, indexOfParan);
										if (methodeName.endsWith("_initialize"))
											initializeMethodName = methodeName;
										else if (methodeName.endsWith("_step"))
											stepMethodName = methodeName;
										else if (methodeName.endsWith("_terminate"))
											terminateMethodName = methodeName;
									}
								}
							}
						}
						extern = null;
					} else
						extern.append(line);
				}
			}

			CStructDescriptor inputs = null;
			CStructDescriptor outputs = null;
			CStructDescriptor parameters = null;
			for (CStructDescriptor struct : structs)
				if (struct.structName.startsWith("ExtU_")) {
					for (Variable variable : externVariable)
						if (variable.type.equals(struct.structName))
							inputs = new CStructDescriptor(struct.structName, variable.name, struct.params);
				} else if (struct.structName.startsWith("ExtY_")) {
					for (Variable variable : externVariable)
						if (variable.type.equals(struct.structName))
							outputs = new CStructDescriptor(struct.structName, variable.name, struct.params);
				} else if (struct.structName.startsWith("P_"))
					for (Variable variable : externVariable)
						if (variable.type.equals(struct.structName))
							parameters = new CStructDescriptor(struct.structName, variable.name, struct.params);
			return new IOPDescriptor(initializeMethodName, stepMethodName, terminateMethodName, inputs, outputs, parameters);
		}
		return null;
	}

	public void process() {
		Object[] addInput = getAdditionalInputs();
		if (addInput == null)
			return;
		ArrayList<PrimitiveVariable> inParams = this.iop.inputs.params;
		this.maxTs = Long.MIN_VALUE;
		for (int i = 0; i < inParams.size(); i++) {
			Object input = addInput[i];
			if (input != null) {
				this.in.writeField(inParams.get(i).name, addInput[i]);
				long ts = getTimeStamp(i);
				if (ts > this.maxTs)
					this.maxTs = ts;
			} else
				addInput[i] = this.oldInputs[i];
		}
		if (this.stepMode != StepMode.FIXED)
			nextStep();
		this.oldInputs = addInput;
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "fixedStepSize", this.stepMode == StepMode.FIXED);
	}

	public void setFixedStepSize(int fixedStepSize) {
		boolean isRunning = stopTimer();
		this.fixedStepSize = fixedStepSize;
		if (isRunning && this.stepMode == StepMode.FIXED)
			try {
				birth();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	public void setHeader(File header) {
		if (Objects.equals(header, this.header))
			return;
		dispose();
		this.header = header;
		reset();
	}

	public void setLibrary(File library) {
		if (Objects.equals(library, this.library))
			return;
		dispose();
		this.library = library;
		reset();
	}

	private void reset() {
		if (isAlive())
			runLater(() -> {
				restartAndReloadStruct();
				updateView();
			});
		else {
			restartAndReloadStruct();
			updateView();
		}
	}

	public void setParameterPrefix(String parameterPrefix) {
		this.parameterPrefix = parameterPrefix;
		dispose();
	}

	public void setParameters(JNAStructure parameters) {
		this.parameters = parameters;
		if (parameters != null && !(parameters instanceof NoParameters)) {
			try {
				parameters.getClass().getMethod("setPointer", Pointer.class).invoke(parameters, this.lib.getGlobalVariableAddress(this.iop.parameters.variablename));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			parameters.write();
		}
	}

	public void setStepMode(StepMode stepMode) {
		boolean isRunning = stopTimer();
		this.stepMode = stepMode;
		if (isRunning && stepMode == StepMode.FIXED)
			try {
				birth();
			} catch (Exception e) {
				e.printStackTrace();
			}
		setEnable();
	}

	private boolean stopTimer() {
		if (this.fixedModeTimer != null) {
			this.fixedModeTimer.shutdownNow();
			this.fixedModeTimer = null;
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void writeClass(StringBuilder sb, String structName, HashMap<String, Object> node, String className, int lvl) {
		// className = className.substring(0, className.length() - 1); //A voir
		String indent = "";
		for (int i = 0; i < lvl; i++)
			indent += "\t";
		className = className + Integer.toString(lvl);
		sb.append(indent + "class " + className + "{\n");
		String newIndent = indent + "\t";

		sb.append(newIndent + "public " + className + "(){}\n");

		HashSet<String> visited = new HashSet<>();
		node.forEach((name, element) -> {
			if (element instanceof HashMap)
				writeClass(sb, structName, (HashMap<String, Object>) element, name, lvl + 1);
			else {
				PrimitiveVariable var = (PrimitiveVariable) element;
				String upName = getVarName(var.name);// .substring(0, 1).toUpperCase() + var.name.substring(1);
				if (!visited.contains(upName) && (this.parameterPrefix == null || var.name.startsWith(this.parameterPrefix))) {
					visited.add(upName);
					String upSimpleName = getVarName(name);// .substring(0, 1).toUpperCase() + name.substring(1); // A voir
					String type = var.type + (var.size == -1 ? "" : "[]");
					sb.append(newIndent + "public " + type + " get" + upSimpleName + "() {\n");
					sb.append(newIndent + "\treturn " + var.name + ";\n");
					sb.append(newIndent + "}\n");
					sb.append(newIndent + "public void set" + upSimpleName + "(" + type + " " + var.name + ") {\n");
					sb.append(newIndent + "\t" + structName + ".this." + var.name + " = " + var.name + ";\n");
					sb.append(newIndent + "\twriteField(\"" + var.name + "\");\n");
					sb.append(newIndent + "}\n");
				}
			}
		});
		visited.clear();
		sb.append(newIndent + "public String toString() {\n");
		sb.append(newIndent + "\treturn \"\";\n");
		sb.append(newIndent + "}\n");
		sb.append(indent + "}\n");
		sb.append(indent + "private " + className + " " + className + " = new " + className + "();\n");
		String upName = getVarName(className);// .substring(0, 1).toUpperCase() + className.substring(1);
		sb.append(indent + "@PropertyInfo(nullable = false)\n");
		sb.append(indent + "@BeanInfo(inline = true)\n");
		sb.append(indent + "public " + className + " get" + upName + "() {\n");
		sb.append(indent + "\treturn " + className + ";\n");
		sb.append(indent + "}\n");
		sb.append(indent + "public void set" + upName + "(" + className + " " + className + ") {\n");
		sb.append(indent + "\tthis." + className + " = " + className + ";\n");
		sb.append(indent + "}\n");
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "parameters", true);
	}

	// @Override
	// public boolean isBlockReady(boolean[] input) {
	// if (stepMode == StepMode.VARIABLEWITHALLENTRIES) {
	// for (boolean b : input)
	// if (!b)
	// return false;
	// return true;
	// } else {
	// for (boolean b : input)
	// if (b)
	// return true;
	// return false;
	// }
	// }
}

class CStructDescriptor {
	public final String structName;
	public final String variablename;
	public final ArrayList<PrimitiveVariable> params;

	public CStructDescriptor(String structName, ArrayList<PrimitiveVariable> params) {
		this.structName = structName;
		this.variablename = null;
		this.params = params;
	}

	public CStructDescriptor(String structName, String variableName, ArrayList<PrimitiveVariable> params) {
		this.structName = structName;
		this.variablename = variableName;
		this.params = params;
	}

	@Override
	public String toString() {
		return this.structName + " " + this.variablename + " " + this.params;
	}
}

class IOPDescriptor {
	public String initMethodName;
	public final String initializeMethodName;
	public final String stepMethodName;
	public final String terminateMethodName;
	public String destroyMethodName;
	public final CStructDescriptor inputs;
	public final CStructDescriptor outputs;
	public final CStructDescriptor parameters;

	public IOPDescriptor(String initializeMethodName, String stepMethodName, String terminateMethodName, CStructDescriptor inputs, CStructDescriptor outputs, CStructDescriptor parameters) {
		if (initializeMethodName == null)
			throw new IllegalArgumentException("No initialize method");
		if (stepMethodName == null)
			throw new IllegalArgumentException("No step method");
		if (terminateMethodName == null)
			throw new IllegalArgumentException("No terminate method");
		if (inputs == null)
			throw new IllegalArgumentException("No input struct");
		if (outputs == null)
			throw new IllegalArgumentException("No output struct");
		// if (parameters == null)
		// throw new IllegalArgumentException("No parameters struct");
		this.initMethodName = null;
		this.initializeMethodName = initializeMethodName;
		this.stepMethodName = stepMethodName;
		this.terminateMethodName = terminateMethodName;
		this.destroyMethodName = null;
		this.inputs = inputs;
		this.outputs = outputs;
		this.parameters = parameters;
	}

	public IOPDescriptor(String initMethodName, String birthMethodName, String processMethodName, String deathMethodName, String destroyMethodName, CStructDescriptor inputs, CStructDescriptor outputs,
			CStructDescriptor parameters) {
		this(birthMethodName, processMethodName, deathMethodName, inputs, outputs, parameters);
		this.initMethodName = initMethodName;
		this.destroyMethodName = destroyMethodName;
	}

	@Override
	public String toString() {
		return "inputs: " + this.inputs + "outputs: " + this.outputs + "parameters: " + this.parameters;
	}
}

class PrimitiveVariable {
	public static final int NOTANARRAY = -1;
	public static final int DYNAMICARRAYSIZE = -2;
	public static final int DYNAMICARRAY = -3;
	public final String name;
	public Class<?> type;
	public int size;

	public PrimitiveVariable(String name, Class<?> type, int size) {
		this.name = name;
		this.type = type;
		this.size = size;
	}

	public void setAsArraySize() {
		this.size = DYNAMICARRAYSIZE;
	}

	public void setAsDynamicArray() {
		this.size = DYNAMICARRAY;
	}

	@Override
	public String toString() {
		return this.name + (this.size == -1 ? "" : "[" + this.size + "]") + " " + this.type;
	}
}

class Variable {
	public final String name;
	public final String type;
	public final int size;

	public Variable(String name, String type, int size) {
		this.name = name;
		if (type.startsWith("P_") && type.endsWith("_T"))
			type = type + "_";
		this.type = type;
		this.size = size;
	}

	@Override
	public String toString() {
		return this.name + (this.size == -1 ? "" : "[" + this.size + "]") + " " + this.type;
	}
}
