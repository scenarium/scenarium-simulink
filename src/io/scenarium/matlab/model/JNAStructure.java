/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.matlab.model;

import java.util.List;

import com.sun.jna.Structure;

public abstract class JNAStructure extends Structure {

	public JNAStructure() {}

	@Override
	protected List<String> getFieldOrder() {
		return List.of();
	}
}
