/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.matlab.model;

import com.sun.jna.Structure;

public abstract class JNAOutput extends Structure {

	public JNAOutput() {}

	public abstract Object read(int index);

}
