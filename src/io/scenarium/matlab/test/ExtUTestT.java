/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.matlab.test;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class ExtUTestT extends Structure {
	public int oursInt32;
	public byte chienBoolean;
	public double vacheDouble;
	public double[] moule1000double = new double[1000];

	public ExtUTestT() {
		super();
	}

	public byte getChienBoolean() {
		return this.chienBoolean;
	}

	@Override
	protected List<String> getFieldOrder() {
		return Arrays.asList("ours_int32", "chien_boolean", "vache_double", "moule_1000_double");
	}

	public double[] getMoule1000Double() {
		return this.moule1000double;
	}

	public int getOursInt32() {
		return this.oursInt32;
	}

	public double getVacheDouble() {
		return this.vacheDouble;
	}

	public void setChienBoolean(byte chienBoolean) {
		this.chienBoolean = chienBoolean;
		writeField("chien_boolean");
	}

	public void setMoule1000Double(double[] moule1000Double) {
		this.moule1000double = moule1000Double;
		writeField("moule_1000_double");
	}

	public void setOursInt32(int oursInt32) {
		this.oursInt32 = oursInt32;
		writeField("ours_int32");
	}

	public void setPointer(Pointer p) {
		useMemory(p);
	}

	public void setVacheDouble(double vacheDouble) {
		this.vacheDouble = vacheDouble;
		writeField("vache_double");
	}
}